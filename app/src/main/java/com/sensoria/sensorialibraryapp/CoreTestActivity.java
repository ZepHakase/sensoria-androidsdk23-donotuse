package com.sensoria.sensorialibraryapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.sensoria.sensorialibrary.SACore;
import com.sensoria.sensorialibrary.SADataPoint;
import com.sensoria.sensorialibrary.SADevice;
import com.sensoria.sensorialibrary.SADeviceInterface;


public class CoreTestActivity extends AppCompatActivity implements SADeviceInterface {

    SACore core;
    private final int interval = 1000;
    private int packetLostCount = 0;
    private int lastTick = 0;
    TextView sampleRate, packetsLost;
    TextView tick, s0, s1, s2, accX, accY, accZ;

    SADataPoint localDataPoint;

    private Handler handler;

    private Runnable runnable = new Runnable() {
        public void run() {

            packetsLost.setText(String.format("%d", packetLostCount));
            packetLostCount = 0;

//            sampleRate.setText(String.format("%d Hz", localDataPoint.actualSamplingFrequency));
//            tick.setText(String.format("%d", localDataPoint.tick));
//            s0.setText(String.format("%d", localDataPoint.channels[0]));
//            s1.setText(String.format("%d", localDataPoint.channels[1]));
//            s2.setText(String.format("%d", localDataPoint.channels[2]));
//            accX.setText(String.format("%f", localDataPoint.accelerometer[0]));
//            accY.setText(String.format("%f", localDataPoint.accelerometer[1]));
//            accZ.setText(String.format("%f", localDataPoint.accelerometer[2]));

            handler.postDelayed(runnable, interval);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_core_test);

        core = new SACore(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        core.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        core.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        core.disconnect();
    }

    public void onTestService(View view) {
        Intent intent = new Intent(this, TestAnkletServiceActivity.class);
        intent.putExtra("macAddr", selectedMac);
        startActivity(intent);
    }

    public void onStartScan(View view) {
        core.startScan();
    }

    public void onStopScan(View view) {
        core.stopScan();
    }

    public void onConnect(View view) {

        Log.w("SensoriaLibrary", "Connect to " + selectedCode + " " + selectedMac);
        core.deviceCode = selectedCode;
        core.deviceMac = selectedMac;
        core.connect();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String selectedCode;
    private String selectedMac;

    @Override
    public void didDiscoverDevice() {

        Log.w("SensoriaLibrary", "Device Discovered!");

        Spinner s = (Spinner) findViewById(R.id.spinner);
        sampleRate = (TextView) findViewById(R.id.sampleRateValue);
        packetsLost = (TextView) findViewById(R.id.packetsLostValue);
        tick = (TextView) findViewById(R.id.tickValue);
        s0 = (TextView) findViewById(R.id.s0Value);
        s1 = (TextView) findViewById(R.id.s1Value);
        s2 = (TextView) findViewById(R.id.s2Value);
        accX = (TextView) findViewById(R.id.accXValue);
        accY = (TextView) findViewById(R.id.accYValue);
        accZ = (TextView) findViewById(R.id.accZValue);

        handler = sampleRate.getHandler();

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, core.deviceDiscoveredList);
        s.setAdapter(adapter);

        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                SADevice deviceDiscovered = core.deviceDiscoveredList.get(position);
                selectedCode = deviceDiscovered.deviceCode;
                selectedMac = deviceDiscovered.deviceMac;

                Log.d("SensoriaLibrary", selectedCode + " " + selectedMac);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedCode = null;
            }
        });
    }

    @Override
    public void didConnect() {

        Log.w("SensoriaLibrary", "Device Connected!");
        //handler.postAtTime(runnable, System.currentTimeMillis() + interval);
        handler.postDelayed(runnable, interval);

    }

    @Override
    public void didError(String message) {

        Log.e("SensoriaLibrary", message);

    }

    @Override
    public void didUpdateData(SADataPoint dataPoint) {
//      Uncomment these lines to get a refresh for each sample received
        sampleRate.setText(String.format("%d Hz", dataPoint.actualSamplingFrequency));
        tick.setText(String.format("%d", dataPoint.tick));
        s0.setText(String.format("%d", dataPoint.channels[0]));
        s1.setText(String.format("%d", dataPoint.channels[1]));
        s2.setText(String.format("%d", dataPoint.channels[2]));
        accX.setText(String.format("%f", dataPoint.accelerometer[0]));
        accY.setText(String.format("%f", dataPoint.accelerometer[1]));
        accZ.setText(String.format("%f", dataPoint.accelerometer[2]));

        // Do something with the data
        localDataPoint = dataPoint;

        if (lastTick != 0) {
            int deltaTick = dataPoint.tick - lastTick - 1;
            if (deltaTick > 0) {
                packetLostCount += deltaTick;
            }
        }
        lastTick = dataPoint.tick;
    }
}
