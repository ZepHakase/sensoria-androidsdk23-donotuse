package com.sensoria.sensorialibraryapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onTestCoreClick(View view) {
        Intent intent = new Intent(this, CoreTestActivity.class);
        startActivity(intent);
    }

    public void onTestAnkletClick(View view) {
        Intent intent = new Intent(this, AnkletTestActivity.class);
        startActivity(intent);
    }
}
