package com.sensoria.wearapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sensoria.sensoriadroidjni.SignalProcessing;
import com.sensoria.sensorialibrary.SAAnkletService;

public class MainActivity extends Activity {

    private TextView stepView;
    private TextView macAddr;

    private final static String TAG = MainActivity.class.getSimpleName();

    private SAAnkletService mBluetoothLeService;

    private SignalProcessing signalProcessing;


    private boolean mIsBound = false;

    class AnkletBroadcastReceiver extends BroadcastReceiver {

        private static final String tag = "BroadcastReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getIntExtra("Type", SAAnkletService.GENERIC_MESSAGE)) {
                case SAAnkletService.CONNECT_MESSAGE:
                    Log.w(tag, "onConnect");
                    break;
                case SAAnkletService.DISCOVER_MESSAGE:
                    Log.w(tag, "onDiscover");
                    break;
                case SAAnkletService.ERROR_MESSAGE:
                    String message = intent.getExtras().getString("Message");
                    Log.w(tag, "onError: " + message);
                    break;
                case SAAnkletService.DATA_MESSAGE:
                    //Log.w(tag, "onDataStreaming");

                    double[] rawDataBuffer = new double[6];

                    //MTB5 S0
                    //MTB1 S1
                    //Heel S2

                    rawDataBuffer[0] = (double) intent.getIntExtra("mtb5", -1);
                    rawDataBuffer[1] = (double) intent.getIntExtra("mtb1", -1);
                    rawDataBuffer[2] = (double) intent.getIntExtra("heel", -1);
                    rawDataBuffer[3] = (double) intent.getFloatExtra("accX", 0.0f);
                    rawDataBuffer[4] = (double) intent.getFloatExtra("accY", 0.0f);
                    rawDataBuffer[5] = (double) intent.getFloatExtra("accZ", 0.0f);

                    signalProcessing.processIncomingData(rawDataBuffer);

                    stepView.setText(String.format("%d", (int)signalProcessing.getStepCount()));

                default:
            }
        }
    }

    private AnkletBroadcastReceiver receiver;
    private ServiceConnection bleServiceConnection;

    @Override
    public void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

        unbindService(bleServiceConnection);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                stepView = (TextView) stub.findViewById(R.id.stepView);

                macAddr = (TextView) stub.findViewById(R.id.macAddrView);

//                macAddr.setText("00:07:80:06:7A:33");    //Default 0097 Anklet
//                macAddr.setText("00:07:80:06:61:2B");    //Default 0417 Anklet
                macAddr.setText("00:07:80:06:4E:C8");    //Default 0260 Anklet
            }
        });

        signalProcessing = new SignalProcessing();

        IntentFilter filter = new IntentFilter(SAAnkletService.broadcastAction);
        receiver = new AnkletBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);

        // Code to manage Service lifecycle.
        bleServiceConnection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName componentName, IBinder service) {
                mBluetoothLeService = ((SAAnkletService.LocalBinder) service).getService();
                if (!mBluetoothLeService.initialize()) {
                    Toast.makeText(getApplicationContext(), "Unable to initialize Service", Toast.LENGTH_SHORT).show();
                    finish();
                }
                // Automatically connects to the device upon successful start-up initialization.
                //mBluetoothLeService.connect(mDeviceAddress);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                mBluetoothLeService = null;
            }
        };

        Intent deviceIntent = new Intent(this, SAAnkletService.class);
        bindService(deviceIntent, bleServiceConnection, BIND_AUTO_CREATE);
        mIsBound = true;
    }

    public void onStartStreaming(View view) {
        mBluetoothLeService.connect(macAddr.getText().toString());
    }

    public void onStopStreaming(View view) {
        stepView.setText("STEPS");
        mBluetoothLeService.disconnect();
    }

}
