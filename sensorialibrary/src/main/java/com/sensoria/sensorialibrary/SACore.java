package com.sensoria.sensorialibrary;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Maurizio Macagno on 6/24/2016
 */
public class SACore extends SADevice implements BluetoothAdapter.LeScanCallback  {

    public boolean connected = false;

    public SADataPoint dataPoint = new SADataPoint();
    public short battery = 0;

    public ArrayList<SADevice> deviceDiscoveredList = new ArrayList<SADevice>();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mConnectedGatt; //mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_SCANNING = 3;

    public final static UUID UUID_SENSORIA_CORE_STREAMING_SERVICE = UUID.fromString("1CAC0001-656E-696C-4B5F-6E6572726157");
    public final static UUID UUID_SENSORIA_CORE_STREAMING_CHARACTERISTIC = UUID.fromString("1CAC0003-656E-696C-4B5F-6E6572726157");

    private static final UUID UUID_BATTERY_SERVICE = UUID.fromString("0000180F-0000-1000-8000-00805F9B34FB");
    private static final UUID UUID_BATTERY_CHARACTERISTIC = UUID.fromString("00002A19-0000-1000-8000-00805F9B34FB");

    private SADeviceInterface iDevice;
    private Context callerContext;

    Handler handler;

    private final static String TAG = SACore.class.getSimpleName();

    private int lastTick = 0;
    private int actualSamplingRate = 0;
    private int oneSecondPacketCount = 0;
    private long oneSecondStartMillis = 0;

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    public SACore(SADeviceInterface delegate)
    {
        // Save the event object for later use.
        iDevice = delegate;
        callerContext = (Context)delegate;

        handler = new Handler();

        BluetoothManager manager = (BluetoothManager) callerContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = manager.getAdapter();

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            //Bluetooth is disabled
            iDevice.didError("No LE Support.");
            return;
        }
    }

    public void resume() {
        if (!callerContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            iDevice.didError("No LE Support.");
            return;
        }
    }

    public void pause() {
        mBluetoothAdapter.stopLeScan(this);
    }

    public void connect() {
        if (mConnectionState == STATE_SCANNING) {
            mBluetoothAdapter.stopLeScan(this);
        }
        mConnectionState = STATE_CONNECTING;

        if (deviceMac == null) {
            //Connect re-scanning devices
            deviceDiscoveredList.clear();
            mBluetoothAdapter.startLeScan(this);
        }
        else {
            //Connect to passed Mac Address
//            // Previously connected device.  Try to reconnect.
//            if (mBluetoothGatt != null) {
//                Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
//                if (mBluetoothGatt.connect()) {
//                    return;
//                } else {
//                    return;
//                }
//            }

            final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(deviceMac);
            if (device == null) {
                iDevice.didError("Device not found. Unable to connect.");
                return;
            }

            connectOnTheMainThread(callerContext, device);
        }
    }

    public void disconnect() {
        mConnectionState = STATE_DISCONNECTED;

        //Disconnect from any active tag connection
        if (mConnectedGatt != null) {
            mConnectedGatt.disconnect();
            mConnectedGatt = null;
        }
    }

    public void startScan() {
        mConnectionState = STATE_SCANNING;
        deviceDiscoveredList.clear();
        mBluetoothAdapter.startLeScan(this);
    }

    public void stopScan() {
        mConnectionState = STATE_DISCONNECTED;
        mBluetoothAdapter.stopLeScan(this);
    }

    /* BluetoothAdapter.LeScanCallback */

    private boolean foundDeviceCodeInArray(String foundDeviceCode) {

        for (SADevice storedDeviceDiscovered : deviceDiscoveredList) {
            if (storedDeviceDiscovered.deviceCode.equals(foundDeviceCode)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {

        String deviceName = device.getName();
        String foundDeviceMac = device.getAddress();

        if (deviceName != null && !deviceName.isEmpty() && deviceName.startsWith("Sensoria-C1-")) {
            String foundDeviceCode = deviceName.substring(12);

            if (!foundDeviceCodeInArray(foundDeviceCode)) {
                SADevice deviceDiscovered = new SADevice();

                deviceDiscovered.deviceCode = foundDeviceCode;
                deviceDiscovered.deviceMac = foundDeviceMac;

                deviceDiscoveredList.add(deviceDiscovered);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        iDevice.didDiscoverDevice();
                    }
                });

                if (mConnectionState == STATE_CONNECTING && deviceMac == null && deviceCode.equals(foundDeviceCode)) {
                    stopScan();

                    Log.i(TAG, "Connecting to " + foundDeviceCode + " after scan");
                    connectOnTheMainThread(callerContext, device);
                }
            }
        }
    }

    private void connectOnTheMainThread(final Context context, final BluetoothDevice device) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectedGatt = device.connectGatt(context, false, mGattCallback);
            }
        });
    }

    private BluetoothGattCallback mGattCallback;

    {
        mGattCallback = new BluetoothGattCallback() {

            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                Log.d(TAG, "Connection State Change: " + status + " -> " + connectionState(newState));
                if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_CONNECTED) {
                /*
                 * Once successfully connected, we must next discover all the services on the
                 * device before we can read and write their characteristics.
                 */
                    gatt.discoverServices();
                } else if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_DISCONNECTED) {
                /*
                 * If at any point we disconnect, send a message to clear the weather values
                 * out of the UI
                 */
                } else if (status != BluetoothGatt.GATT_SUCCESS) {
                /*
                 * If there is a failure at any stage, simply disconnect
                 */
                    gatt.disconnect();
                }
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                Log.d(TAG, "Services Discovered: " + status);

                //TODO: Battery coverage

                if (status == BluetoothGatt.GATT_SUCCESS) {
                    // Loops through available GATT Services to look for Sensoria Core Streaming Service
                    for (BluetoothGattService gattService : gatt.getServices()) {
                        if (gattService.getUuid().compareTo(UUID_SENSORIA_CORE_STREAMING_SERVICE) == 0) {
                            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();

                            // Loops through available Characteristics to find streaming service
                            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                                if (gattCharacteristic.getUuid().compareTo(UUID_SENSORIA_CORE_STREAMING_CHARACTERISTIC)== 0) {
                                    final int charaProp = gattCharacteristic.getProperties();

                                    // Confirm that this supports notify
                                    if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                                        setCharacteristicNotification(gattCharacteristic, true);

                                        mConnectionState = STATE_CONNECTED;  //TODO: JACOPO: Check if not just set in onConnectionStateChange

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                iDevice.didConnect();
                                            }
                                        });
                                        break;
                                    }
                                }
                            }

                            break;
                        }
                    }
                }
            }

            /**
             * Enables or disables notification on a give characteristic.
             *
             * @param characteristic Characteristic to act on.
             * @param enabled        If true, enable notification.  False otherwise.
             */
            private void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                                       boolean enabled) {
                if (mBluetoothAdapter == null || mConnectedGatt == null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            iDevice.didError("BluetoothAdapter not initialized");
                        }
                    });

                    return;
                }

                mConnectedGatt.setCharacteristicNotification(characteristic, enabled);

                byte[] enableNotification = (enabled) ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE;

                UUID uuidCharacteristic = characteristic.getUuid();

                Log.d(TAG, "setCharacteristicNotification: UUID: " + uuidCharacteristic.toString());

                // This is specific to Heart Rate Measurement.
                //if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
                //BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));

                List<BluetoothGattDescriptor> bluetoothGattDescriptors = characteristic.getDescriptors();

                if (bluetoothGattDescriptors == null || bluetoothGattDescriptors.size() == 0) {
                    return;
                }

                BluetoothGattDescriptor descriptor = bluetoothGattDescriptors.get(0);
                //BluetoothGattDescriptor descriptor = bluetoothGattDescriptors.get(1);

                descriptor.setValue(enableNotification);
                mConnectedGatt.writeDescriptor(descriptor);
                //}
            }


            @Override
            public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
                Log.d(TAG, "Remote RSSI: " + rssi);
            }

            private String connectionState(int status) {
                switch (status) {
                    case BluetoothProfile.STATE_CONNECTED:
                        return "Connected";
                    case BluetoothProfile.STATE_DISCONNECTED:
                        return "Disconnected";
                    case BluetoothProfile.STATE_CONNECTING:
                        return "Connecting";
                    case BluetoothProfile.STATE_DISCONNECTING:
                        return "Disconnecting";
                    default:
                        return String.valueOf(status);
                }
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt,
                                             final BluetoothGattCharacteristic characteristic,
                                             int status) {

                Log.d(TAG, "onCharacteristicRead");

                if (status == BluetoothGatt.GATT_SUCCESS) {


                }
            }

            private void setSamplingFrequency(byte[] data) {
                dataPoint.samplingFrequency = ((data[1] & 0x03) == 0 ? 32 : 64);
            }

            private int getAccelerometerRangeIndex(byte[] data) {
                return ((data[1] >>> 2) & 0x03);
            }

            private int getGyroscopeRangeIndex(byte[] data) {
                return ((data[1] >>> 4) & 0x03);
            }

            private int getMagnetometerRangeIndex(byte[] data) {
                return ((data[1] >>> 6) & 0x03);
            }

            private float accelToFloat(int rangeIndex, short rawValue) {
                // These are range lookup for 16 bits
                //float[] lookup = {0.0000610352f, 0.00012207f, 0.000244141f, 0.000488281f };

                // We actually use 10 bits - Shifting left by 2^6
                float[] lookup = {0.0000610352f * 64, 0.00012207f * 64, 0.000244141f * 64, 0.000488281f * 64};
                float conversion = lookup[rangeIndex];

                return (conversion * (rawValue < 512 ? rawValue : -(1024 - rawValue)));
            }

            private float gyroToFloat(int rangeIndex, short rawValue) {
                // These are range lookup for 16 bits
                //float[] lookup = { 0.00875f, 0.0175f, 0.035f, 0.070f };

                // We actually use 10 bits - Shifting left by 2^6
                float[] lookup = { 0.00875f * 64, 0.0175f * 64, 0.035f * 64, 0.070f * 64 };
                float conversion = lookup[rangeIndex];

                return (conversion * (rawValue < 512 ? rawValue : -(1024 - rawValue)));
            }

            private float magnetToFloat(int rangeIndex, short rawValue)
            {
                // These are range lookup for 16 bits
                //float[] lookup = { 0.000146f, 0.000292f, 0.000438f, 0.000584f };

                // We actually use 10 bits - Shifting left by 2^6
                float[] lookup = { 0.000146f * 64, 0.000292f * 64, 0.000438f * 64, 0.000584f * 64 };
                float conversion = lookup[rangeIndex];

                return (conversion * (rawValue < 512 ? rawValue : -(1024 - rawValue)));
            }

            private void didUpdateData() {
                final SADataPoint cloned = dataPoint.clone();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        iDevice.didUpdateData(cloned);
                    }
                });
            }
            
            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt,
                                                final BluetoothGattCharacteristic characteristic) {
                //TODO: Battery characteristic notify

                if (UUID_SENSORIA_CORE_STREAMING_CHARACTERISTIC.equals(characteristic.getUuid())) {
                    byte[] data = characteristic.getValue().clone();
                    int dataLength = data.length;
                    
                    if (dataLength == 20) {
                        int messageType = (data[0] >>> 4) & 0x0F;

                        int gRangeIndex = 0;
                        int dpsRangeIndex = 0;
                        int gaussRangeIndex = 0;

                        // Fucking Java does not have unsigned data types, so we need extra bit masks all over in case byte values are negative - masking & 0xFF makes the byte an integer and eliminates the sign

                        switch (messageType) {
                            case 0x0D: // D20
                                setSamplingFrequency(data);

                                dataPoint.channels[0] = (short)(0x3FF & (((data[5]  & 0xFF) << 2) | ((data[6]  & 0xFF)  >>> 6)));  //sensor0
                                dataPoint.channels[1] = (short)(0x3FF & (((data[6]  & 0xFF) << 4) | ((data[7]  & 0xFF)  >>> 4)));  //sensor1
                                dataPoint.channels[2] = (short)(0x3FF & (((data[7]  & 0xFF) << 6) | ((data[8]  & 0xFF)  >>> 2)));  //sensor2
                                dataPoint.channels[3] = (short)(0x3FF & (((data[8]  & 0xFF) << 8) | ((data[9]  & 0xFF)       )));  //sensor3
                                dataPoint.channels[4] = (short)(0x3FF & (((data[10] & 0xFF) << 2) | ((data[11] & 0xFF)  >>> 6)));  //sensor4
                                dataPoint.channels[5] = (short)(0x3FF & (((data[11] & 0xFF) << 4) | ((data[12] & 0xFF)  >>> 4)));  //sensor5
                                dataPoint.channels[6] = (short)(0x3FF & (((data[12] & 0xFF) << 6) | ((data[13] & 0xFF)  >>> 2)));  //sensor6
                                dataPoint.channels[7] = (short)(0x3FF & (((data[13] & 0xFF) << 8) | ((data[14] & 0xFF)       )));  //sensor7

                                dataPoint.tick = (data[2] & 0xFF) | ((data[3] & 0xFF) << 8) | ((data[4] & 0xFF) << 16);
                                didUpdateData();
                                break;
                            case 0x0E: // E20
                                setSamplingFrequency(data);

                                dataPoint.channels[0] = (short)(0x3FF & (((data[5]  & 0xFF) << 2) | ((data[6]  & 0xFF)  >>> 6)));  //sensor0
                                dataPoint.channels[1] = (short)(0x3FF & (((data[6]  & 0xFF) << 4) | ((data[7]  & 0xFF)  >>> 4)));  //sensor1
                                dataPoint.channels[2] = (short)(0x3FF & (((data[7]  & 0xFF) << 6) | ((data[8]  & 0xFF)  >>> 2)));  //sensor2
                                dataPoint.channels[3] = (short)(0x3FF & (((data[8]  & 0xFF) << 8) | ((data[9]  & 0xFF)       )));  //sensor3
                                dataPoint.channels[4] = (short)(0x3FF & (((data[10] & 0xFF) << 2) | ((data[11] & 0xFF)  >>> 6)));  //sensor4
                                dataPoint.channels[5] = (short)(0x3FF & (((data[11] & 0xFF) << 4) | ((data[12] & 0xFF)  >>> 4)));  //sensor5
                                dataPoint.channels[6] = 0;
                                dataPoint.channels[7] = 0;

                                gRangeIndex = getAccelerometerRangeIndex(data);
                                dataPoint.accelerometer[0] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[12] & 0xFF) << 6) | ((data[13] & 0xFF) >>> 2))));
                                dataPoint.accelerometer[1] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[13] & 0xFF) << 8) | ((data[14] & 0xFF)      ))));
                                dataPoint.accelerometer[2] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[15] & 0xFF) << 2) | ((data[16] & 0xFF) >>> 6))));

                                dpsRangeIndex = getGyroscopeRangeIndex(data);
                                dataPoint.gyroscope[0] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[16] & 0xFF) << 4) | ((data[17] & 0xFF) >>> 4))));
                                dataPoint.gyroscope[1] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[17] & 0xFF) << 6) | ((data[18] & 0xFF) >>> 2))));
                                dataPoint.gyroscope[2] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[18] & 0xFF) << 8) | ((data[19] & 0xFF)      ))));

                                dataPoint.magnetometer[0] = 0.0f;
                                dataPoint.magnetometer[1] = 0.0f;
                                dataPoint.magnetometer[2] = 0.0f;

                                dataPoint.tick = (data[2] & 0xFF) | ((data[3] & 0xFF) << 8) | ((data[4] & 0xFF) << 16);
                                didUpdateData();
                                break;
                            case 0x0F: // F20
                                setSamplingFrequency(data);

                                dataPoint.channels[0] = (short)(0x3FF & (((data[5]  & 0xFF) << 2) | ((data[6]  & 0xFF)  >>> 6)));  //sensor0
                                dataPoint.channels[1] = (short)(0x3FF & (((data[6]  & 0xFF) << 4) | ((data[7]  & 0xFF)  >>> 4)));  //sensor1
                                dataPoint.channels[2] = (short)(0x3FF & (((data[7]  & 0xFF) << 6) | ((data[8]  & 0xFF)  >>> 2)));  //sensor2
                                dataPoint.channels[3] = 0;
                                dataPoint.channels[4] = 0;
                                dataPoint.channels[5] = 0;
                                dataPoint.channels[6] = 0;
                                dataPoint.channels[7] = 0;

                                gRangeIndex = getAccelerometerRangeIndex(data);
                                dataPoint.accelerometer[0] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[12] & 0xFF) << 6) | ((data[13] & 0xFF) >>> 2))));
                                dataPoint.accelerometer[1] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[13] & 0xFF) << 8) | ((data[14] & 0xFF)      ))));
                                dataPoint.accelerometer[2] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[15] & 0xFF) << 2) | ((data[16] & 0xFF) >>> 6))));

                                dpsRangeIndex = getGyroscopeRangeIndex(data);
                                dataPoint.gyroscope[0] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[16] & 0xFF) << 4) | ((data[17] & 0xFF) >>> 4))));
                                dataPoint.gyroscope[1] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[17] & 0xFF) << 6) | ((data[18] & 0xFF) >>> 2))));
                                dataPoint.gyroscope[2] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[18] & 0xFF) << 8) | ((data[19] & 0xFF)      ))));

                                gaussRangeIndex = getMagnetometerRangeIndex(data);
                                dataPoint.magnetometer[0] = magnetToFloat(gaussRangeIndex, (short)(0x3FF & (((data[8]  & 0xFF) << 8) | ((data[9]  & 0xFF)      ))));
                                dataPoint.magnetometer[1] = magnetToFloat(gaussRangeIndex, (short)(0x3FF & (((data[10] & 0xFF) << 2) | ((data[11] & 0xFF) >>> 6))));
                                dataPoint.magnetometer[2] = magnetToFloat(gaussRangeIndex, (short)(0x3FF & (((data[11] & 0xFF) << 4) | ((data[12] & 0xFF) >>> 4))));

                                dataPoint.tick = (int)data[2] | ((int)data[3] << 8) | ((int)(data[4] << 16));
                                didUpdateData();
                                break;
                            case 0x01: // G20
                                setSamplingFrequency(data);

                                dataPoint.channels[0] = (short)(0x3FF & (((data[5]  & 0xFF) << 2) | ((data[6]  & 0xFF)  >>> 6)));  //sensor0
                                dataPoint.channels[1] = (short)(0x3FF & (((data[6]  & 0xFF) << 4) | ((data[7]  & 0xFF)  >>> 4)));  //sensor1
                                dataPoint.channels[2] = (short)(0x3FF & (((data[7]  & 0xFF) << 6) | ((data[8]  & 0xFF)  >>> 2)));  //sensor2
                                dataPoint.channels[3] = (short)(0x3FF & (((data[8]  & 0xFF) << 8) | ((data[9]  & 0xFF)       )));  //sensor3
                                dataPoint.channels[4] = (short)(0x3FF & (((data[10] & 0xFF) << 2) | ((data[11] & 0xFF)  >>> 6)));  //sensor4
                                dataPoint.channels[5] = (short)(0x3FF & (((data[11] & 0xFF) << 4) | ((data[12] & 0xFF)  >>> 4)));  //sensor5
                                dataPoint.channels[6] = (short)(0x3FF & (((data[0]  & 0xFF) << 6) | ((data[1]  & 0xC0)  >>> 2) | ((data[4] & 0x0F))));  //sensor6
                                dataPoint.channels[7] = 0;

                                gRangeIndex = getAccelerometerRangeIndex(data);
                                dataPoint.accelerometer[0] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[12] & 0xFF) << 6) | ((data[13] & 0xFF) >>> 2))));
                                dataPoint.accelerometer[1] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[13] & 0xFF) << 8) | ((data[14] & 0xFF)      ))));
                                dataPoint.accelerometer[2] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[15] & 0xFF) << 2) | ((data[16] & 0xFF) >>> 6))));

                                dpsRangeIndex = getGyroscopeRangeIndex(data);
                                dataPoint.gyroscope[0] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[16] & 0xFF) << 4) | ((data[17] & 0xFF) >>> 4))));
                                dataPoint.gyroscope[1] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[17] & 0xFF) << 6) | ((data[18] & 0xFF) >>> 2))));
                                dataPoint.gyroscope[2] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[18] & 0xFF) << 8) | ((data[19] & 0xFF)      ))));

                                dataPoint.magnetometer[0] = 0.0f;
                                dataPoint.magnetometer[1] = 0.0f;
                                dataPoint.magnetometer[2] = 0.0f;

                                dataPoint.tick = (data[2] & 0xFF) | ((data[3] & 0xFF) << 8) | ((data[4] & 0xF0) << 12);
                                didUpdateData();
                                break;
                            case 0x02: // H20
                                setSamplingFrequency(data);

                                dataPoint.channels[0] = (short)(0x3FF & (((data[5]  & 0xFF) << 2) | ((data[6]  & 0xFF)  >>> 6)));  //sensor0
                                dataPoint.channels[1] = (short)(0x3FF & (((data[6]  & 0xFF) << 4) | ((data[7]  & 0xFF)  >>> 4)));  //sensor1
                                dataPoint.channels[2] = (short)(0x3FF & (((data[7]  & 0xFF) << 6) | ((data[8]  & 0xFF)  >>> 2)));  //sensor2
                                dataPoint.channels[3] = (short)(0x3FF & (((data[0]  & 0xFF) << 6) | ((data[4] & 0x3F))));          //sensor3
                                dataPoint.channels[4] = 0;
                                dataPoint.channels[5] = 0;
                                dataPoint.channels[6] = 0;
                                dataPoint.channels[7] = 0;

                                gRangeIndex = getAccelerometerRangeIndex(data);
                                dataPoint.accelerometer[0] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[12] & 0xFF) << 6) | ((data[13] & 0xFF) >>> 2))));
                                dataPoint.accelerometer[1] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[13] & 0xFF) << 8) | ((data[14] & 0xFF)      ))));
                                dataPoint.accelerometer[2] = accelToFloat(gRangeIndex, (short)(0x3FF & (((data[15] & 0xFF) << 2) | ((data[16] & 0xFF) >>> 6))));

                                dpsRangeIndex = getGyroscopeRangeIndex(data);
                                dataPoint.gyroscope[0] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[16] & 0xFF) << 4) | ((data[17] & 0xFF) >>> 4))));
                                dataPoint.gyroscope[1] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[17] & 0xFF) << 6) | ((data[18] & 0xFF) >>> 2))));
                                dataPoint.gyroscope[2] = gyroToFloat(dpsRangeIndex, (short)(0x3FF & (((data[18] & 0xFF) << 8) | ((data[19] & 0xFF)      ))));

                                gaussRangeIndex = getMagnetometerRangeIndex(data);
                                dataPoint.magnetometer[0] = magnetToFloat(gaussRangeIndex, (short)(0x3FF & (((data[8]  & 0xFF) << 8) | ((data[9]  & 0xFF)      ))));
                                dataPoint.magnetometer[1] = magnetToFloat(gaussRangeIndex, (short)(0x3FF & (((data[10] & 0xFF) << 2) | ((data[11] & 0xFF) >>> 6))));
                                dataPoint.magnetometer[2] = magnetToFloat(gaussRangeIndex, (short)(0x3FF & (((data[11] & 0xFF) << 4) | ((data[12] & 0xFF) >>> 4))));

                                dataPoint.tick = (data[2] & 0xFF) | ((data[3] & 0xFF) << 8) | ((data[4] & 0xC0) << 10);
                                didUpdateData();
                                break;

                            default:
                                Log.d(TAG, "Protocol not supported" + messageType);
                                break;
                        }
                    }
                    else {
                        Log.d(TAG, "Unexpected protocol length: " + dataLength);
                    }

                    if (lastTick != 0) {
                        int deltaTick = dataPoint.tick - lastTick;
                        if (deltaTick > 1) {
                            Log.d(TAG, String.format("Packets lost: %d", deltaTick - 1));
                        }
                    }
                    Log.d(TAG, String.format("tick: %d",dataPoint.tick));
                    lastTick = dataPoint.tick;

                    long timestamp = System.currentTimeMillis();

                    if (timestamp - oneSecondStartMillis > 1000) {
                        oneSecondStartMillis = timestamp;
                        actualSamplingRate = oneSecondPacketCount;
                        oneSecondPacketCount = 0;
                    }

                    dataPoint.actualSamplingFrequency = actualSamplingRate;
                    ++ oneSecondPacketCount;

                } else {
                    // For all other profiles, writes the data formatted in HEX.
                    final byte[] data = characteristic.getValue();
                    if (data != null && data.length > 0) {
                        final StringBuilder stringBuilder = new StringBuilder(data.length);
                        for (byte byteChar : data)
                            stringBuilder.append(String.format("%02X ", byteChar));

                        Log.d(TAG, new String(data) + "\n" + stringBuilder.toString());
                    }
                }
            }
        };
    }
}
