package com.sensoria.sensorialibrary;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mac on 6/24/2016.
 */
public class SADataPoint implements Parcelable {

    public int[] channels = new int[8];
    public float[] accelerometer = new float[3];
    public float[] gyroscope = new float[3];
    public float[] magnetometer = new float[3];
    public int tick = 0;
    public int samplingFrequency;
    public int actualSamplingFrequency;

    public SADataPoint() {}

    public SADataPoint clone() {
        SADataPoint object = new SADataPoint();
        object.tick = tick;
        object.samplingFrequency = samplingFrequency;
        object.actualSamplingFrequency = actualSamplingFrequency;
        object.channels = channels.clone();
        object.accelerometer = accelerometer.clone();
        object.gyroscope = gyroscope.clone();
        object.magnetometer = magnetometer.clone();

        return object;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeIntArray(channels);
        dest.writeFloatArray(accelerometer);
        dest.writeFloatArray(gyroscope);
        dest.writeFloatArray(magnetometer);
        dest.writeInt(tick);
    }

    private SADataPoint(Parcel in) {
        in.readIntArray(channels);
        in.readFloatArray(accelerometer);
        in.readFloatArray(gyroscope);
        in.readFloatArray(magnetometer);
        tick = in.readInt();
    }

    public static final Parcelable.Creator<SADataPoint> CREATOR = new Parcelable.Creator<SADataPoint>() {
        public SADataPoint createFromParcel(Parcel in) {
            return new SADataPoint(in);
        }

        public SADataPoint[] newArray(int size) {
            return new SADataPoint[size];
        }
    };
}
